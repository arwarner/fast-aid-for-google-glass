package glass.newmedia.fastaid;

import java.util.Timer;
import java.util.TimerTask;
 
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognizerIntent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;
import android.content.Context;
import android.graphics.Color;
import android.view.MotionEvent;
import android.speech.tts.TextToSpeech;

import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
import com.google.android.glass.app.Card;
import com.google.android.glass.media.Sounds;

import android.media.AudioManager;


public class FastAidActivity extends Activity {
    
	private TextToSpeech mSpeech;
	private AudioManager audio;
	private Timer p;
    private double STAGE = 0;
    private GestureDetector mGestureDetector;
    private String emptyspeech = "";
    
    //first card - is the person conscious or unconcious?
    private String card1 = "Is the victim conscious or unconscious?";
    private String card1answer1 = "Unconscious";
    private String card1answer2 = "Conscious";
    private String speech1 = "Is the victim conscious? Swipe to select";
    
    //second card - is the person breathing or not?
    private String card2 = "Is the victim breathing?";
    private String card2answer1 = "No";
    private String card2answer2 = "Yes";
    private String speech2 = "Check to see if the person is breathing. Swipe to select";
    
    //third card - if the person is concious, we won't implement anything for that right now
    private String card3 = "More first aid support forthcoming";
    private String speech3 = "Right now, we haven't implemented support for all first aid training. Look for a later version that implements all first aid methods taught by the Red Cross!";
    
    //the fourth card - if the person is breathing but not conscious, go here
    private String card4 = "Maintain an open airway";
    private String card4imagestep2 = "picture11";
    private String card4step3 = "Continue to observe";
    private String speech4 = "Maintain an open airway by tilting the head back and lifting the chin.";
    private String speech4step2 = "This diagram shows the proper way to open the airway.";
    private String speech4step3 = "Continue to monitor for life threatening conditions until Emergency Response comes. If the person's situation changes, swipe back and select appropriate first aid.";
   
    //the fifth card - if the user is unconcious and not breathing. inform the user they need to perform CPR
    private String card5 = "Perform CPR";
    private String speech5 = "The victim is in need of C P R";
    
    //the 6th card - call 911
    private String card6 = "Call 911";
    private String speech6 = "Instruct someone around you to call nine one one, or if you are alone, call nine one one. Tap when finished.";
    
    //the 7th card - how old is the victim?
    private String card7 = "What age group is the victim?";
    private String card7answer1 = "Adult";
    private String card7answer2 = "Child";
    private String card7answer3 = "Infant";
    private String speech7 = "CPR methods vary based on age. Swipe to select the age group of the victim.";
    
    //the 8th card - this loops through the CPR Steps as divided up for the demo.
    //in the real app, everything from this point on will automatically forward onto the next method on a timer
    
    //this section is getting into CPR position
    private String card8 = "Assume CPR Position";
    private String speech8 = "Kneel beside the person's upper chest";
    private String card8imagepart2 = "picture14";
    private String speech8part2 = "Place the heel of one hand on the person�s sternum at the center of the chest";
    private String card8imagepart3 = "picture15";
    private String speech8part3 = "Place your other hand directly on top of the first hand. Keep your fingers lifted off the chest";
    private String card8imagepart4 = "picture16";
    private String speech8part4 = "Keep your arms and elbows as straight as possible, and your shoulders directly over your hands";
    
    //this section describes the CPR compressions
    private String card9 = "Give 30 chest compressions";
    private String speech9 = "Give 30 chest compressions. Each compression should push the sternum down at least two inches.";
    private String speech9part2= "Listen for a counter to keep your pace";
    
    //this section describes the rescue breaths
    private String card10 = "Give 2 rescue breaths";
    private String speech10 = "Give two rescue breaths";
    private String card10imagepart2 = "picture11";
    private String speech10part2 = "Open the airway by tilting the head back and lifting the chin.";
    private String card10imagepart3 = "picture20";
    private String speech10part3 = "Pinch the nose shut then make a complete seal over the person�s mouth.";
    private String card10imagepart4 = "picture21";
    private String speech10part4 = "Each rescue breath should last about 1 second and make the chest clearly rise";
    
    //card 11 - starts the constant loop at the end of CPR
    private String card11image = "picture22";
    private String speech11 = "Continue cycles of chest compressions and rescue breaths. Once you have begun C P R, do not stop until you notice an obvious sign of life such as breathing, a defibrillator is available for use, or emergency response personnel arrive at the scene.";
    
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGestureDetector = createGestureDetector(this);
        audio = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        mSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                // Do nothing.
            	//mSpeech.speak(speech1, TextToSpeech.QUEUE_FLUSH, null);
            	setCard(card1,speech1);
                STAGE = 1;
            }
        });
        
    }
    
    private void setCard (String body, String speech){
        Card card = new Card(this);
        CharSequence cs = body;
        card.setText(cs);
        setContentView(card.getView());
        mSpeech.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
    }
    
    private void setImageCard(String body, String speech, String image){
    	Card card = new Card(this);
    	CharSequence cs = body;
        card.setText(cs);
    	card.setImageLayout(Card.ImageLayout.FULL);
    	int resID = getResources().getIdentifier(image , "drawable", getPackageName());
    	card.addImage(resID);
    	setContentView(card.getView());
        mSpeech.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
    }
    
    private void setTimerCard(String body, String speech, int timeout, String image){
    	Card card = new Card(this);
        card.setText(body);
        if (image != null && !image.isEmpty()){
        	card.setImageLayout(Card.ImageLayout.FULL);
        	int resID = getResources().getIdentifier(image , "drawable", getPackageName());
        	card.addImage(resID);
        }
        setContentView(card.getView());
        mSpeech.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
        
        Handler handler = new Handler(); 
        handler.postDelayed(new Runnable() { 
             public void run() { 
            	 if (STAGE == 4.0){
            		 setTimerCard("", speech4step2, 5000, card4imagestep2);
            		 STAGE = 4.1;
            	 }
            	 else if (STAGE == 4.1){
            		 setCard(card4step3, speech4step3);
            		 STAGE = 4.2;
            	 }
            	 else if (STAGE == 5.0){
            		 setCard(card6, speech6);
            		 STAGE = 6.0;
            	 }
            	 else if (STAGE == 8.0){
            		 setTimerCard("", speech8part2, 5000, card8imagepart2);
            		 STAGE = 8.1;
            	 }
            	 else if (STAGE == 8.1){
            		 setTimerCard("", speech8part3, 6000, card8imagepart3);
            		 STAGE = 8.2;
            	 }
            	 else if (STAGE == 8.2){
            		 setTimerCard("", speech8part4,8000, card8imagepart4);
            		 STAGE = 9;
            	 }
            	 else if (STAGE == 9){
            		 setTimerCard(card9, speech9 ,6000, "");
            		 STAGE= 9.1;
            	 }
            	 else if (STAGE == 9.1){
             		compressionAction();
             	 }
            	 else if (STAGE == 10.0){
            		 setTimerCard("",  speech10part2, 5000, card10imagepart2);
            		 STAGE = 10.1;
            	 }
            	 else if (STAGE == 10.1){
            		 setTimerCard("", speech10part3, 6000, card10imagepart3);
            		 STAGE = 10.2;
            	 }
            	 else if (STAGE == 10.2){
            		 setTimerCard("", speech10part4, 8000, card10imagepart4);
            		 STAGE = 10.3;
            	 }
            	 else if (STAGE == 10.3){
             		setTimerCard("", speech11, 17000, card11image);
             		STAGE = 11.0;
             	}
             	else if (STAGE == 11){
            		CPRForever();
            		STAGE = 11.1;
            	}
           } 
        }, timeout);
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        // Provide a way to exit the program
        if (keyCode == KeyEvent.KEYCODE_BACK) {
        	//this is when you swipe the back button. input all the back steps here.
        	if (STAGE == 1){
                this.finish();
                return false;
            }
        	else if (STAGE == 1.1 || STAGE == 1.2){
        		setCard(card1, speech1);
        		STAGE = 1.0;
        	}
        	else if (STAGE == 2 || STAGE == 2.1 || STAGE == 2.2){
        		setCard(card1, speech1);
        		STAGE = 1.0;
        	}
        	else if (STAGE == 3.0){
        		setCard(card1, speech1);
        		STAGE = 1.0;
        	}
        	else if (STAGE >=4.0 && STAGE < 4.5){
        		setCard(card2, speech2);
        		STAGE = 2.0;
        	}
        	else if (STAGE == 5.0){
        		setCard(card2, speech2);
        		STAGE = 2.0;
        	}
        	else if (STAGE == 6){
        		setCard(card5, speech5);
        		STAGE = 5.0;
        	}
        	else if (STAGE >= 7.0 && STAGE < 7.5){
        		setCard(card6, speech6);
        		STAGE = 6.0;
        	}
        	else if (STAGE >= 8.0 && STAGE < 8.5){
        		setCard(card7, speech7);
        		STAGE = 7.0;
        	}
        	else if (STAGE == 9.0 || STAGE == 9.1){
        		setCard(card7, speech7);
        		STAGE = 7.0;
        	}
        	else if (STAGE >= 10.0 && STAGE < 10.5){
        		setCard(card7, speech7);
        		STAGE = 7.0;
        	}
        	else if (STAGE == 11 || STAGE == 11.1){
        		p.cancel();
                this.finish();
                return false;
        	}
        	
        }
 
        // If a user taps the track pad
        if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
            
        }
 
    return false;
    }//END ONKEYDOWN
    
     private GestureDetector createGestureDetector(Context context) {
            GestureDetector gestureDetector = new GestureDetector(context);
                //Create a base listener for generic gestures
                gestureDetector.setBaseListener( new GestureDetector.BaseListener() {
                    @Override
                    public boolean onGesture(Gesture gesture) {
                        if (gesture == Gesture.TAP) {
                        	if (STAGE == 1.2){
                        		setCard(card3, speech3);
                        		STAGE = 3.0;
                            }
                        	else if (STAGE == 1.1){
                        		setCard(card2, speech2);
                        		STAGE = 2.0;
                        	}
                        	else if (STAGE == 2.2){
                        		setTimerCard(card4, speech4, 6000, "");
                            	STAGE = 4.0;
                        	}
                        	else if (STAGE == 2.1){
                        		setTimerCard(card5, speech5, 3000, "");
                        		STAGE = 5.0;
                        	}
                        	else if (STAGE == 6.0){
                        		setCard(card7, speech7);
                        		STAGE = 7.0;
                        	}
                        	else if (STAGE == 7.1 || STAGE == 7.2 || STAGE == 7.3){
                        		setTimerCard(card8, speech8, 4000, "");
                        		STAGE = 8.0;
                        	}
                        	
                        	return true;
                        } 
                        else if (gesture == Gesture.TWO_TAP) {
                            // do something on two finger tap
                            return true;
                        } 
                        else if (gesture == Gesture.SWIPE_RIGHT) {
                            // do something on right (forward) swipe
                        	if (STAGE == 1.0){
                            	setCard(card1answer1, emptyspeech);
                            	STAGE = 1.1;
                            }
                            else if (STAGE == 1.1){
                            	setCard(card1answer2, emptyspeech);
                            	STAGE = 1.2;
                            }
                            else if (STAGE == 1.2){
                            	setCard(card1, speech1);
                            	STAGE = 1.0;
                            }
                            else if (STAGE == 2.0){
                            	setCard(card2answer1, emptyspeech);
                            	STAGE = 2.1;
                            }
                            else if (STAGE == 2.1){
                            	setCard(card2answer2, emptyspeech);
                            	STAGE = 2.2;
                            }
                            else if (STAGE == 2.2){
                            	setCard(card2, speech2);
                            	STAGE = 2.0;
                            }
                            else if (STAGE == 7.0){
                            	setCard(card7answer1, emptyspeech);
                            	STAGE = 7.1;
                            }
                            else if (STAGE == 7.1){
                            	setCard(card7answer2, emptyspeech);
                            	STAGE = 7.2;
                            }
                            else if (STAGE == 7.2){
                            	setCard(card7answer3, emptyspeech);
                            	STAGE = 7.3;
                            }
                            else if (STAGE == 7.3){
                            	setCard(card7, speech7);
                            	STAGE = 7.0;
                            }
                        	return true;
                        } 
                        else if (gesture == Gesture.SWIPE_LEFT) {
                            //do something on left (backwards) swipe
                        	if (STAGE == 1.0){
                        		setCard(card1answer2, emptyspeech);
                            	STAGE = 1.2;
                        	}
                        	else if (STAGE == 1.2){
                        		setCard(card1answer1, emptyspeech);
                            	STAGE = 1.1;
                        	}
                        	else if (STAGE == 1.1){
                        		setCard(card1, speech1);
                            	STAGE = 1.0;
                        	}
                        	else if (STAGE == 2.0){
                        		setCard(card2answer2, emptyspeech);
                        		STAGE = 2.2;
                        	}
                        	else if (STAGE == 2.2){
                        		setCard(card2answer1, emptyspeech);
                        		STAGE = 2.1;
                        	}
                        	else if (STAGE == 2.1){
                        		setCard(card2, speech2);
                        		STAGE = 2.0;
                        	}
                        	else if (STAGE == 7.0){
                        		setCard(card7answer3, emptyspeech);
                        		STAGE = 7.3;
                        	}
                        	else if (STAGE == 7.3){
                        		setCard(card7answer2, emptyspeech);
                        		STAGE = 7.2;
                        	}
                        	else if (STAGE == 7.2){
                        		setCard(card7answer1, emptyspeech);
                        		STAGE = 7.1;
                        	}
                        	else if (STAGE == 7.1){
                        		setCard(card7, speech7);
                        		STAGE = 7.0;
                        	}
                            return true;
                            
                        }
                        return false;
                    }
                });
                gestureDetector.setFingerListener(new GestureDetector.FingerListener() {
                    @Override
                    public void onFingerCountChanged(int previousCount, int currentCount) {
                      // do something on finger count changes
                    }
                });
                gestureDetector.setScrollListener(new GestureDetector.ScrollListener() {
                    @Override
                    public boolean onScroll(float displacement, float delta, float velocity) {
                        // do something on scrolling
                        return true;
                    }
                });

                return gestureDetector;
            }

            /*
             * Send generic motion events to the gesture detector
             */
            @Override
            public boolean onGenericMotionEvent(MotionEvent event) {
                if (mGestureDetector != null) {
                    return mGestureDetector.onMotionEvent(event);
                }
                return false;
            }
            
            private void compressionAction(){
                Card card = new Card(this);
                card.setText("Give 30 chest compressions");
                setContentView(card.getView());
                Handler handler = new Handler();
                mSpeech.speak(speech9part2, TextToSpeech.QUEUE_FLUSH, null);

                final Timer t = new Timer();
                t.schedule(new TimerTask() {
                	public void run() {
                		audio.playSoundEffect(Sounds.TAP);
                	}
                }, 4000, 600);
                
                handler.postDelayed(new Runnable(){
                	public void run(){
                		t.cancel();
                		setTimerCard(card10, speech10, 3000, "");
                		STAGE = 10.0;
                	}
                }, 22000);
            }
            
            private void CPRForever(){
                Card card = new Card(this);
                card.setText("");
                card.setImageLayout(Card.ImageLayout.FULL);
            	int resID = getResources().getIdentifier(card11image, "drawable", getPackageName());
            	card.addImage(resID);
            	setContentView(card.getView());
                mSpeech.speak("Keep pace with the timer to keep yourself on track.", TextToSpeech.QUEUE_FLUSH, null);
                setContentView(card.getView());
                
                p = new Timer();
               
                //this loops through the click noises. 
                p.schedule(new TimerTask(){
                	public void run(){
                		audio.playSoundEffect(Sounds.TAP);
                	}
                }, 4000, 600);
                //STOP
                p.schedule(new TimerTask(){
                	public void run(){
                		mSpeech.speak("Give breaths now", TextToSpeech.QUEUE_FLUSH, null);
                	}
                }, 24000, 25000);
                p.schedule(new TimerTask(){
                	public void run(){
                		mSpeech.speak("Start compressions now", TextToSpeech.QUEUE_FLUSH, null);
                	}
                }, 4000, 25000);
                
                
                
            }

}